import React,{useEffect} from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

// import Slider from "react-slick";   

export default function Home() {

    useEffect(() => {
        AOS.init();
      }, [])

  return (
    <div class="main-wrapper">
        {/* Header */}
        <header className="header">
        <nav className="navbar navbar-expand-lg header-nav">
            <div className="navbar-header">
            <a id="mobile_btn" href="javascript:void(0);">
                <span className="bar-icon bar-icon-one">
                <span />
                <span />
                <span />
                </span>
            </a>
            <a href="index.html" className="navbar-brand logo">
                <img src="assets/img/logo.png" className="img-fluid" alt="Logo" />
            </a>
            </div>
            <div className="main-menu-wrapper">
            <div className="menu-header">
                <a href="index.html" className="menu-logo">
                <img src="assets/img/logo.png" className="img-fluid" alt="Logo" />
                </a>
                <a id="menu_close" className="menu-close" href="javascript:void(0);">
                <i className="fas fa-times" />
                </a>
            </div>
            <ul className="main-nav">
                <li className="has-submenu active">
                <a href>Home <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li className="active"><a href="index.html">Home</a></li>
                    <li><a href="index-two.html">Home 2</a></li>
                    <li><a href="index-three.html">Home 3</a></li>
                    <li><a href="index-four.html">Home 4</a></li>
                    <li><a href="index-five.html">Home 5</a></li>
                    <li><a href="index-six.html">Home 6</a></li>
                    <li><a href="index-seven.html">Home 7</a></li>
                    <li><a href="index-eight.html">Home 8</a></li>
                    <li><a href="index-nine.html">Home 9</a></li>
                    <li><a href="index-ten.html">Home 10</a></li>
                    <li><a href="index-leven.html">Home 11</a></li>
                    <li><a href="index-twelve.html">Home 12</a></li>
                    <li><a href="index-thirteen.html">Home 13</a></li>
                    <li><a href="index-fourteen.html">Home 14</a></li>
                    <li><a href="index-fifteen.html">Home 15</a></li>
                </ul>
                </li>
                <li className="has-submenu">
                <a href>Doctors <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li><a href="doctor-dashboard.html">Doctor Dashboard</a></li>
                    <li><a href="appointments.html">Appointments</a></li>
                    <li><a href="schedule-timings.html">Schedule Timing</a></li>
                    <li><a href="my-patients.html">Patients List</a></li>
                    <li><a href="patient-profile.html">Patients Profile</a></li>
                    <li><a href="chat-doctor.html">Chat</a></li>
                    <li><a href="invoices.html">Invoices</a></li>
                    <li><a href="doctor-profile-settings.html">Profile Settings</a></li>
                    <li><a href="reviews.html">Reviews</a></li>
                    <li><a href="doctor-register.html">Doctor Register</a></li>
                    <li className="has-submenu">
                    <a href="doctor-blog.html">Blog</a>
                    <ul className="submenu">
                        <li><a href="doctor-blog.html">Blog</a></li>
                        <li><a href="blog-details.html">Blog view</a></li>
                        <li><a href="doctor-add-blog.html">Add Blog</a></li>
                    </ul>
                    </li>
                </ul>
                </li>	
                <li className="has-submenu">
                <a href>Patients <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li className="has-submenu">
                    <a href="#">Doctors</a>
                    <ul className="submenu">
                        <li><a href="map-grid.html">Map Grid</a></li>
                        <li><a href="map-list.html">Map List</a></li>
                        <li><a href="map-list-1.html">Map List 1</a></li>
                    </ul>
                    </li>
                    <li><a href="search.html">Search Doctor</a></li>
                    <li><a href="doctor-profile.html">Doctor Profile</a></li>
                    <li><a href="booking.html">Booking</a></li>
                    <li><a href="checkout.html">Checkout</a></li>
                    <li><a href="booking-success.html">Booking Success</a></li>
                    <li><a href="patient-dashboard.html">Patient Dashboard</a></li>
                    <li><a href="favourites.html">Favourites</a></li>
                    <li><a href="chat.html">Chat</a></li>
                    <li><a href="profile-settings.html">Profile Settings</a></li>
                    <li><a href="change-password.html">Change Password</a></li>
                </ul>
                </li>
                <li className="has-submenu">
                <a href>Pharmacy <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li><a href="pharmacy-index.html">Pharmacy</a></li>
                    <li><a href="pharmacy-details.html">Pharmacy Details</a></li>
                    <li><a href="pharmacy-search.html">Pharmacy Search</a></li>
                    <li><a href="product-all.html">Product</a></li>
                    <li><a href="product-description.html">Product Description</a></li>
                    <li><a href="cart.html">Cart</a></li>
                    <li><a href="product-checkout.html">Product Checkout</a></li>
                    <li><a href="payment-success.html">Payment Success</a></li>
                    <li><a href="pharmacy-register.html">Pharmacy Register</a></li>
                </ul>
                </li>
                <li className="has-submenu">
                <a href>Pages <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li><a href="voice-call.html">Voice Call</a></li>
                    <li><a href="video-call.html">Video Call</a></li>
                    <li><a href="search.html">Search Doctors</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                    <li><a href="onboarding-email.html">Doctor Onboarding</a></li>
                    <li><a href="patient-email.html">Patient Onboarding</a></li>
                    <li><a href="components.html">Components</a></li>
                    <li className="has-submenu">
                    <a href="invoices.html">Invoices</a>
                    <ul className="submenu">
                        <li><a href="invoices.html">Invoices</a></li>
                        <li><a href="invoice-view.html">Invoice View</a></li>
                    </ul>
                    </li>
                    <li><a href="blank-page.html">Starter Page</a></li>
                    <li><a href="about-us.html">About Us</a></li>
                    <li><a href="contact-us.html">Contact Us</a></li>
                    <li><a href="login.html">Login</a></li>
                    <li><a href="register.html">Register</a></li>
                    <li><a href="forgot-password.html">Forgot Password</a></li>
                </ul>
                </li>
                <li className="has-submenu">
                <a href>Blog <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li><a href="blog-list.html">Blog List</a></li>
                    <li><a href="blog-grid.html">Blog Grid</a></li>
                    <li><a href="blog-details.html">Blog Details</a></li>
                </ul>
                </li>
                <li className="has-submenu">
                <a href="#" target="_blank">Admin <i className="fas fa-chevron-down" /></a>
                <ul className="submenu">
                    <li><a href="admin/index.html" target="_blank">Admin</a></li>
                    <li><a href="pharmacy/index.html" target="_blank">Pharmacy Admin</a></li>
                </ul>
                </li>
                <li className="login-link">
                <a href="login.html">Login / Signup</a>
                </li>
            </ul>		 
            </div>		 
            <ul className="nav header-navbar-rht">
            <li className="nav-item contact-item">
                <div className="header-contact-img">
                <i className="far fa-hospital" />							
                </div>
                <div className="header-contact-detail">
                <p className="contact-header">Contact</p>
                <p className="contact-info-header"> +1 315 369 5943</p>
                </div>
            </li>
            <li className="nav-item">
                <a className="nav-link header-login btn-one-light" href="login.html">login / Signup </a>
            </li>
            </ul>
        </nav>
        </header>
{/* /Header */}

{/* Home Banner */}
    <section className="section section-search">
    <div className="container-fluid">
        <div className="banner-wrapper">
        <div className="banner-header text-center aos" data-aos="fade-up">
            <h1>Search Doctor, Make an Appointment</h1>
            <p>Discover the best doctors, clinic &amp; hospital the city nearest to you.</p>
        </div>
        {/* Search */}
        <div className="search-box aos" data-aos="fade-up">
            <form action="search.html">
            <div className="form-group search-location">
                <input type="text" className="form-control" placeholder="Search Location" />
                <span className="form-text">Based on your Location</span>
            </div>
            <div className="form-group search-info">
                <input type="text" className="form-control" placeholder="Search Doctors, Clinics, Hospitals, Diseases Etc" />
                <span className="form-text">Ex : Dental or Sugar Check up etc</span>
            </div>
            <button type="submit" className="btn btn-primary search-btn mt-0"><i className="fas fa-search" /> <span>Search</span></button>
            </form>
        </div>
      {/* /Search */}
        </div>
        </div>
    </section>
{/* /Home Banner */}

<section className="section home-tile-section">
  <div className="container-fluid">
    <div className="row">
      <div className="col-md-9 m-auto">
        <div className="section-header text-center aos" data-aos="fade-up">
          <h2>What are you looking for?</h2>
        </div>
        <div className="row">
          <div className="col-lg-4 mb-3 aos" data-aos="fade-up">
            <div className="card text-center doctor-book-card">
              <img src="assets/img/doctors/doctor-07.jpg" alt className="img-fluid" />
              <div className="doctor-book-card-content tile-card-content-1">
                <div>
                  <h3 className="card-title mb-0">Visit a Doctor</h3>
                  <a href="search.html" className="btn book-btn1 px-3 py-2 mt-3 btn-one-light" tabIndex={0}>Book Now</a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 mb-3 aos" data-aos="fade-up">
            <div className="card text-center doctor-book-card">
              <img src="assets/img/img-pharmacy1.jpg" alt className="img-fluid" />
              <div className="doctor-book-card-content tile-card-content-1">
                <div>
                  <h3 className="card-title mb-0">Find a Pharmacy</h3>
                  <a href="pharmacy-search.html" className="btn book-btn1 px-3 py-2 mt-3 btn-one-light" tabIndex={0}>Find Now</a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 mb-3 aos" data-aos="fade-up">
            <div className="card text-center doctor-book-card">
              <img src="assets/img/lab-image.jpg" alt className="img-fluid" />
              <div className="doctor-book-card-content tile-card-content-1">
                <div>
                  <h3 className="card-title mb-0">Find a Lab</h3>
                  <a href="javascript:void(0);" className="btn book-btn1 px-3 py-2 mt-3 btn-one-light" tabIndex={0}>Coming Soon</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{/* Clinic and Specialities */}
<section className="section section-specialities">
  <div className="container-fluid">
    <div className="section-header text-center aos" data-aos="fade-up">
      <h2>Clinic and Specialities</h2>
      <p className="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div className="row justify-content-center">
      <div className="col-md-9 aos" data-aos="fade-up">
        {/* Slider */}
        <div className="specialities-slider slider">
     
          {/* Slider Item */}
          <div className="speicality-item text-center">
            <div className="speicality-img">
              <img src="assets/img/specialities/specialities-01.png" className="img-fluid" alt="Speciality" />
              <span><i className="fa fa-circle" aria-hidden="true" /></span>
            </div>
            <p>Urology</p>
          </div>	
          {/* /Slider Item */}
          {/* Slider Item */}
          <div className="speicality-item text-center">
            <div className="speicality-img">
              <img src="assets/img/specialities/specialities-02.png" className="img-fluid" alt="Speciality" />
              <span><i className="fa fa-circle" aria-hidden="true" /></span>
            </div>
            <p>Neurology</p>	
          </div>							
          {/* /Slider Item */}
          {/* Slider Item */}
          <div className="speicality-item text-center">
            <div className="speicality-img">
              <img src="assets/img/specialities/specialities-03.png" className="img-fluid" alt="Speciality" />
              <span><i className="fa fa-circle" aria-hidden="true" /></span>
            </div>	
            <p>Orthopedic</p>	
          </div>							
          {/* /Slider Item */}
          {/* Slider Item */}
          <div className="speicality-item text-center">
            <div className="speicality-img">
              <img src="assets/img/specialities/specialities-04.png" className="img-fluid" alt="Speciality" />
              <span><i className="fa fa-circle" aria-hidden="true" /></span>
            </div>	
            <p>Cardiologist</p>	
          </div>							
          {/* /Slider Item */}
          {/* Slider Item */}
          <div className="speicality-item text-center">
            <div className="speicality-img">
              <img src="assets/img/specialities/specialities-05.png" className="img-fluid" alt="Speciality" />
              <span><i className="fa fa-circle" aria-hidden="true" /></span>
            </div>	
            <p>Dentist</p>
          </div>							
          {/* /Slider Item */}
         
        </div>
        {/* /Slider */}
      </div>
    </div>
  </div>   
</section>	 
{/* Clinic and Specialities */}
{/* Popular Section */}
<section className="section section-doctor">
  <div className="container-fluid">
    <div className="row">
      <div className="col-lg-4">
        <div className="section-header aos" data-aos="fade-up">
          <h2>Book Our Doctor</h2>
          <p>Lorem Ipsum is simply dummy text </p>
        </div>
        <div className="about-content aos" data-aos="fade-up">
          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
          <p>web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>					
          <a href="javascript:;">Read More..</a>
        </div>
      </div>
      <div className="col-lg-8">
        <div className="doctor-slider slider aos" data-aos="fade-up">
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-01.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Ruby Perrin</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MDS - Periodontology and Oral Implantology, BDS</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <span className="d-inline-block average-rating">(17)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Florida, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $300 - $1000 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-02.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Darren Elder</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">BDS, MDS - Oral &amp; Maxillofacial Surgery</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(35)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Newyork, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $50 - $300 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-03.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Deborah Angel</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MBBS, MD - General Medicine, DNB - Cardiology</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(27)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Georgia, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $100 - $400 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-04.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Sofia Brient</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MBBS, MS - General Surgery, MCh - Urology</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(4)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Louisiana, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $150 - $250 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-05.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Marvin Campbell</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MBBS, MD - Ophthalmology, DNB - Ophthalmology</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(66)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Michigan, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $50 - $700 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-06.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Katharine Berthold</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MS - Orthopaedics, MBBS, M.Ch - Orthopaedics</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(52)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Texas, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $100 - $500 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-07.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Linda Tobin</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MBBS, MD - General Medicine, DM - Neurology</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(43)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> Kansas, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $100 - $1000 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* /Doctor Widget */}
          {/* Doctor Widget */}
          <div className="profile-widget">
            <div className="doc-img">
              <a href="doctor-profile.html">
                <img className="img-fluid" alt="User Image" src="assets/img/doctors/doctor-08.jpg" />
              </a>
              <a href="javascript:void(0)" className="fav-btn">
                <i className="far fa-bookmark" />
              </a>
            </div>
            <div className="pro-content">
              <h3 className="title">
                <a href="doctor-profile.html">Paul Richard</a> 
                <i className="fas fa-check-circle verified" />
              </h3>
              <p className="speciality">MBBS, MD - Dermatology , Venereology &amp; Lepros</p>
              <div className="rating">
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star filled" />
                <i className="fas fa-star" />
                <span className="d-inline-block average-rating">(49)</span>
              </div>
              <ul className="available-info">
                <li>
                  <i className="fas fa-map-marker-alt" /> California, USA
                </li>
                <li>
                  <i className="far fa-clock" /> Available on Fri, 22 Mar
                </li>
                <li>
                  <i className="far fa-money-bill-alt" /> $100 - $400 
                  <i className="fas fa-info-circle" data-bs-toggle="tooltip" title="Lorem Ipsum" />
                </li>
              </ul>
              <div className="row row-sm">
                <div className="col-6">
                  <a href="doctor-profile.html" className="btn view-btn">View Profile</a>
                </div>
                <div className="col-6">
                  <a href="booking.html" className="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          {/* Doctor Widget */}
        </div>
      </div>
    </div>
  </div>
</section>
{/* /Popular Section */}
  
			


    </div>
  )
}
